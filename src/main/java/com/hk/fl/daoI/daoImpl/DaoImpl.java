package com.hk.fl.daoI.daoImpl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.hk.fl.daoI.DaoI;
import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;
import com.hk.fl.util.TempMapper;
import com.hk.fl.util.UserMapper;

/**
 * 
 * @author Harish Kalepu 02/19/2020
 *
 */
@Repository
public class DaoImpl implements DaoI {

	private Logger logger = LoggerFactory.getLogger(DaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public User authUser(User user) throws Exception {
		String query = "SELECT * FROM users where id = ? and password = ?";
		User result = (User) jdbcTemplate.queryForObject(query, new Object[] { user.getUserId(), user.getPassword() },
				new UserMapper());
		return result;
	}

	@Override
	public List<TempTable> uploadFile(ArrayList<TempTable> file1) throws Exception {
		
		for (TempTable emp : file1) {
			System.out.println("ID:" + emp.getId() + " firstName:" + emp.getName());
		}
		int[][] counts = batchInsert(file1, 200);
		List<TempTable> result = jdbcTemplate.query("SELECT * FROM temp_table", new TempMapper());
		return result;
	}

	public int[][] batchInsert(List<TempTable> tempTable, int batchSize) {

		int[][] updateCounts = jdbcTemplate.batchUpdate("insert into temp_table (id, name, address) values(?,?,?)",
				tempTable, batchSize, new ParameterizedPreparedStatementSetter<TempTable>() {
					public void setValues(PreparedStatement ps, TempTable argument) throws SQLException {
						ps.setString(1, argument.getId());
						ps.setString(2, argument.getName());
						ps.setString(3, argument.getAddress());
					}
				});
		return updateCounts;

	}
	
	@Override
	public List<TempTable> getData(String value) throws Exception {
		return jdbcTemplate.query("SELECT * FROM temp_table where name like '%"+value+"%' or address like '%"+value+"%'", new TempMapper());
	}
}
