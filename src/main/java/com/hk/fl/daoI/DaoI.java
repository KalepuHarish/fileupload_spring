package com.hk.fl.daoI;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;

/**
 * 
 * @author Harish Kalepu 02/19/2020
 *
 */
public interface DaoI {
	
	public User authUser(User user) throws Exception;
	
	public List<TempTable> uploadFile(ArrayList<TempTable> file) throws Exception;
	
	public List<TempTable> getData(String value) throws Exception;
}
