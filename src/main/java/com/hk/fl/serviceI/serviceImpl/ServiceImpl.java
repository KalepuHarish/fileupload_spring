package com.hk.fl.serviceI.serviceImpl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hk.fl.daoI.DaoI;
import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;
import com.hk.fl.serviceI.ServiceI;

/**
 * 
 * @author Harish Kalepu 02/19/2020
 *
 */
@Service
@Transactional
public class ServiceImpl implements ServiceI {

	@Autowired
	private DaoI daoI;

	@Override
	public User authUser(User user) throws Exception {
		return daoI.authUser(user);
	}

	@Override
	public List<TempTable> uploadFile(InputStream file) throws Exception {
		FileInputStream file1 = (FileInputStream) file;
		// Create Workbook instance holding reference to .xlsx file
		XSSFWorkbook workbook = new XSSFWorkbook(file1);
		// Get first/desired sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);
		ArrayList<TempTable> tempList = new ArrayList<>();
		ArrayList<String> headervalue =new ArrayList<String>();
		int k=sheet.getFirstRowNum();
		Row header = sheet.getRow(k);
		for (int l = header.getFirstCellNum(); l <= header.getLastCellNum(); l++) {
			Cell ce = header.getCell(l); 
			if(ce != null)
				headervalue.add(ce.getStringCellValue());
			else
				break;
		}
		System.out.print("header value" + headervalue);
		if(headervalue.get(0).equals("ID") && headervalue.get(1).equals("NAME") && headervalue.get(2).equals("ADDRESS"))
		{
			// I've Header and I'm ignoring header for that I've +1 in loop
			for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++) {
				TempTable e = new TempTable();
				Row ro = sheet.getRow(i);
				for (int j = ro.getFirstCellNum(); j <= ro.getLastCellNum(); j++) {
					Cell ce = ro.getCell(j);
					if (j == 0) {
						// If you have Header in text It'll throw exception because it won't get
						e.setId(String.valueOf(ce.getNumericCellValue()));
					}
					if (j == 1) {
						e.setName(ce.getStringCellValue());
					}
					if (j == 2) {
						e.setAddress(ce.getStringCellValue());
					}
				}
				tempList.add(e);
			}
			return daoI.uploadFile(tempList);
		}
		
		return null;
	}

	@Override
	public List<TempTable> getData(String value) throws Exception {
		return daoI.getData(value);
	}

}
