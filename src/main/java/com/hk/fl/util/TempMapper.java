package com.hk.fl.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hk.fl.domain.TempTable;

public class TempMapper implements RowMapper {

	@Override
	public TempTable mapRow(ResultSet rs, int rowNum) throws SQLException {
		TempTable tempTable = new TempTable();
		tempTable.setId(rs.getString("id"));
		tempTable.setName(rs.getString("name"));
		tempTable.setAddress(rs.getString("address"));
		return tempTable;
	}

}

