CREATE TABLE IF NOT EXISTS  users  (
   ID  varchar(25) NOT NULL,
   NAME  varchar(50) NOT NULL,
   PASSWORD  varchar(200) NOT NULL,
  PRIMARY KEY ( ID )
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE IF NOT EXISTS  temp_table  (
   ID  varchar(25) NOT NULL,
   NAME  varchar(50) NOT NULL,
   ADDRESS  varchar(200) NOT NULL,
  PRIMARY KEY ( ID )
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

INSERT INTO users values('HarishK1991','Harish Kalepu','Google@19');